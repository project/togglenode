<?php

function togglenode_admin() {
	$form['togglenode'] = array(
		'#value' => t("You must add the following line of text to your page.tpl.php file in order to make this module work: " . check_plain("<?php print \$togglelinks; ?>")),
	);
	
	$form['togglenode_view'] = array(
		'#type' => 'radios',
		'#title' => t("Exlude 'View' from the Toggle Node?"),
		'#description' => t("If excluded, the tab 'View' will not be shown"),
		'#default_value' => variable_get("togglenode_view", 1),
		'#options' => array(
			0 => t("No, do not exlude the View link"),
			1 => t("Yes, exclude the View link"),
		),
	);
	
	return system_settings_form($form);
}